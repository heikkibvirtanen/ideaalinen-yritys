
* Konstruktiivinen työnhaku lyhyesti

** Päämäärä

Yksiselitteinen päämäärä on löytää itse ja auttaa muita verkostoon kuuluvia työnhakijoita löytämään *timanttinen unelmaduuni*, joka on kaiken työnhakuun käytetyn vaivan ja energian arvoinen.

** Periaatteita

Työttömyys ja työnhaku ovat palvelutuotantoa, jonka seurauksena niin yhteiskunta, liike-elämä kuin vertaisetkin saavat konkreettista lisäarvoa. Ja aivan kuten kaikkeen muuhunkin taloudelliseen toimintaan, myös työnhaussa on aina esillä ja osana tarjoomuksia tekijöiden arvot ja etiikka.

Ihmistä ei määritä se, mitä hän on aikaisemmin tehnyt ja mitä osaa nyt, vaan se, kuka hän haluaa olla ja mitä saada elämässään aikaan.

Kun ihminen voi hyvin, hän on täynnä energiaa. Kun energian suuntaa oikein, syntyy huipputulos. Työnhaussakin. (Aki Hintsa)

Työttömäksi jääminen ja työllistyminen eivät ole omia valintoja, vaan päätökset tekee aina joku muu.

Tekijältä ei voi samanaikaisesti vaatia tiettyä lopputulosta *ja* käskeä häntä hoitamaan homma määrätyllä tavalla. Se on aina joko tai.

** Taustavaikuttimia

Työttömyys ei ole ongelma vaan liike-elämän ja talouspolitiikan huonojen valintojen ja päätösten seuraus. Mutta silti työttömyydestä puhutaan kuin se olisi maan suru ja kaikkien yhteiskunnallisten ongelmien äiti. Ja työttömiä pidetään vanhasta muistista ilmaisten lounaiden perään kärkkyvinä epäluotettavina siipeilijöinä. Heidän pitää tehdä raporttia tekemisistään ja käydä säännöllisesti ilmoittautumassa viranomaisille kuin ehdonalaisessa olevat rikolliset.

Työnhakuvalmennuskin lähtee sanattomasta oletuksesta, että palkallisia töitä riittää kaikille. Pitää vain olla viimeisen päälle tehdyt työnhakuasiakirjat, olla aktiivinen, soitella kuin paraskin puhelinmyyjä ja markkinoida omaa osaamistaan. Ei. Työmarkkina on alati muuttuva pelikenttä, jossa on aina jotain osaamista liian vähän ja toista taas liikaa tarjolla.

Työttömienkin kannalta mielenkiintoisia ovat vaikeuksia kokoneiden tai jopa epäonnistuneiden yrittäjien tarinat. Yllättävän usein niissä toistuvat ihmisiin liittyvät vaikeudet. Yhteystyö kariutuu henkilökohtaisiin ristiriitoihin. Tehtäväjako jää jollekin epäselväksi tai sitoutuminen puolitiehen, joten olennaisia asioita jää hoitamatta. Liian merkittävään asemaan päätyy henkilö, joka luulee itsestään ja merkityksestään yritykselle liikoja. Pahimmat seuraukset henkilöihin liittyvistä ongelmista on pienissä yrityksissä, mutta vähän isommatkaan eivät ole turvassa.

Ihmisiin liittyviä ongelmia pystyy välttämään hyvällä toiminta- ja johtamiskulttuurilla, ja vuorovaikutusta tukevalla toimintaympäristöllä. Ongelma on, että hyvän toimintakulttuurin syntyminen ja vakiintuminen vaatii kaikilta osallisilta tietoja ja taitoja, joita ei voi opetella ja harjoitella oikeastaan missään. Suomalainen koulutus keskittyy ammattiosaamiseen ja työelämätaidot jää oman harrastuneisuuden varaan, ja isoissa korporaatioissa on 1950 ja 60-lukujen autotehtaiden liukuhihnalta tuttu meininki.

Muutamat uuden ajan yritykset ovat lähteneet kokeilemaan kehittyneempiä toimintakulttuureja. Tyypillisesti nämä yritykset ovat uusia eli toimintakulttuuria on päästy luomaan puhtaalta pöydältä. Eivätkä kyseiset yritykset ole toimintakulttuureiltaan niin vakiintuneitakaan, että pystyisivät sopeuttamaan ja hyväksymään kenet tahansa joukkoonsa.

[[./Kuvia/organisaation_kehitystasot.png]]

Toimintakulttuurit ovat siitä mielenkiintoinen tutkimisen ja opettelun kohde, että mikä tahansa asialle omistautunut yhteisö riittää toimintaympäristöksi. Mitään varsinaista liikeideaa tai toimivaa yritystä ei tarvita, ei ainakaan heti aluksi. Työnhaku ja sen käytäntöjen kehittäminen riittää vallan mainiosti harjoitusvastustajaksi. Ja mikä parasta, työnhaun sivutuotteena tulee opiskelleeksi hyödyllisiä liiketoiminnan kehittämisen taitoja.

* Timanttista unelmaduunia etsimässä

Mistä tunnistaa hyvän työpaikan? Miten sellaisen voisi löytää ja vielä saada? Voiko sellaista edes olla olemassa vai pitääkö sittenkin tyytyä siihen, mitä sattuu saamaan? Hankalia kysymyksiä, joihin ei ole helppo vastata. Eikä vähiten siksi, että oma mielemme ryhtyy helposti kapinaan ja alkaa perustella, miksi ei kannata, tai ohjaa käyttämään jokun toisen mielikuvia unelmaduunin määrittelyssä.

Mutta mietitäänpä. Rekrytoijat painottavat motivaatiota ja innostuneisuutta keskeisinä valintakriteereinä. Siksi on kokolailla pelkkää oman aikansa ja energiansa haaskausta hakea hommaa, joka ei pätkääkään kiinnosta. Tietysti rekrytointiprosessin kuluessa on aina mahdollista tehdä ainutkertaisia havaintoja ja oppia jotain uutta ja mielenkiintoista, mutta pidemmän päälle palkkään haastatteluun tyytyminen ei ole kannattavaa.

Joillekin aikaisempi ura ja vaatimattomat saavutukset ovat este työllistymiselle, koska aikaisempia saavutuksia käytetään myös valintakriteerinä. Tämä tosin ruokkii yhtä yhteiskuntaa muutenkin piinaavaa systeemistä vääristymää, jossa aikaisemmat, lähinnä sattuman sanelemat voitot ja häviöt lähtevät kertautumaan ja jakamaan kansaa kahtia hyväosaisiin ja toistuvasti ohitettuihin. Tällaiseen ansaan joutumista ei ole helppo huomata ja häviäjien on siitä vaikeaa ja työlästä hypätä pois. Lähinnä tulee mieleen, että hankkii todistettavasti sellaista yrityksen kilpailukykyä lisäävää tietoa ja osaamista, jota kukaan ei halua ainakaan kilpailijan hyödyntävän. Tulevaisuuden toimintakulttuurit ja yhteistyön muodot eivät välttämättä ole huono vaihtoehto tähän tarkoitukseen.

Yksi este alkuunpääsylle on, että on kovin monta asiaa, jotka /voi/ tehdä. Tähän ongelmaan on luonnollinen ratkaisu, että asettaa itselleen tavoitteen jota kohti pyrkiä. Silloin tekemiselle tulee suunta ja tietää aina, mitä /täytyy/ tehdä seuraavaksi, jotta pääsee lähemmäksi tavoitetta. Ja jos joku asia vie lähemmäksi timanttista unelmaduunia, niin se vie kyllä lähemmäksi muitakin työpaikkoja.

#+ATTR_LATEX: :width 5cm
#+CAPTION: Saku Tuomisen esityksestä ``Parempaa johtajuutta 925 Timantti -mallilla''
[[./Kuvia/timanttinen_duuni.svg.png]]

Oman timanttisen unelmaduunin määrittelyssä on sekin etu, että alkaa tunnistaa myös työt, joihin ei ole kannattavaa sotkeutua. Eikä nyt ole kysymys pelkästään rahasta. Yllättävän monessa organisaatiossa toiminta- ja johtamiskulttuuri on niin pahasti rikki, että se sairastuttaa, jopa lopulliselle sairaseläkkeelle asti. Myös omien arvojen ja etiikan unohtaminen rahaa vastaan kyynistää ja rikkoo sielun.

Pelkästään lukemalla ja mielikuvaharjoituksilla on mahdoton saada selville, millainen oma timanttinen unelmaduuni on ja millaisesta organisaatiosta sen voisi löytää. Siksi mieleen tulleiden ideoiden ja hypoteesien validointiin ja testaamiseen tarvitaan kokemusperäistä tietoa, vertailukohtia ja syväluotaavia analyysejä teorioita vastaan.

Koska olemassa olevissa yrityksissä käsitellään liikesalaisuuksia eikä testaaminen tuotannossa ole koskaan fiksu idea, niin aidossa ympäristössä tehtävät kokeilut eivät ole työttömille vaihtoehto, eivät ainakaan aluksi. Aidon oman yrityksenkin perustaminen on työttömälle niin iso taloudellinen riski, ettei sitä pelkän oppimisen takia kannata ottaa. Mutta mikään ei estä luomasta mallia ideaalisesta yrityksestä ja testata se toimimalla yhteisönä kuin toimisimme yrityksenä.

* Mikä on yritys?

Virallisen määritelmän mukaan yrityksellä tarkoitetaan yhden tai usean henkilön yhdessä harjoittamaa taloudellista toimintaa, joka tähtää kannattavaan tulokseen
([[http://www.stat.fi/meta/kas/yritys.html]]). Tuo ei kuitenkaan luo mielikuvaa, millainen yritys on reaalimaailman käsittein ja mitä taloudellinen toiminta käytännössä tarkoittaa. Kirjallisuudesta on onneksi löydettävissä muutama valaiseva näkökulma aiheeseen.

** Rajapinta

Erään yrittäjävalmennuksen valmentaja Rami Lehtinen kuvasi yritystä rajapinnaksi. Liiketoiminnan harjoittajalla on velvoitteita yhteiskunnan suuntaan ja ainakin osittain vapaaehtoisia vastuita asiakkaiden, työntekijöiden ja omistajien suuntaan. Sillä sinänsä ei ole merkitystä, kuka yrityksen sisällä velvoitteista ja vastuista huolehtii ja miten, kunhan ne tulevat hoidettua.

Tämä on se näkökulma, mikä määrittää, miksi yritys olemassa. Toisin sanoen löytää vastaukset kysymyksiin, mitä ongelmaa yritys ratkaisee ja kenen ongelma se on.

Tällä tasolla näkyvät myös yrityksen arvot ja etiikka. Esimerkiksi erään vanhan ja menestyneen yrityksen arvot jakautuvat prioriteettijärjestyksessä seuraaviin kolmeen luokkaan.

1. Yritys tukee sitä ympäröivän yhteisön ja yhteiskunnan vakautta tuottamalla reaalitaloudellista lisäarvoa.

2. Työntekijät ja yhteistyökumppanit palkitaan yrityksen eteen tehdystä työstä yllätyksettömällä ja inhimisellä kohtelulla, turvallisuudella ja hyvää elämää edistävällä taloudellisella vakaudella.

3. Asiakkaille luodaan sellaista lisäarvoa, jota muut toimijat eivät tarjoa ja josta he ovat valmiit yrityksen palkitsemaan.

Timanttisen unelmaduunin kannalta tästä näkökulmasta johtuu työn merkityksellisyys ja yhteensopivuus oman arvomaailman kanssa sekä työn kannattavuus oman ja perheen elämän kannalta.

*Huomaa: Arvoilla on aina hinta, mutta mielipiteet ovat ilmaisia.* Esimerkiksi jos pitää luonnon monimuotoisuutta ja tulevaisuudenkin elämänedellytyksiä aidosti arvossa, luopuu omaa mukavuutta lisäävistä resurssisyöpöistä hyödykkeistä.

#+ATTR_LATEX: :width 8cm
#+CAPTION: Intiaanin viisaus
[[./Kuvia/intiaanin_viisaus.jpg]]


** Joukko ihmisiä tekemässä yhteistyötä

Yksinyrittäjien mikroyritykset ovat poikkeus, mutta muuten yritys elää ja voi hyvin vain omistajien, johtajien ja työntekijöiden yhteistyön ansiosta. Mutta kuten tunnettua, yhdessä tekeminen on vaikea laji, joka tarvitsee onnistuakseen ainakin tehokkaan vuorovaikutuksen mahdollistavan toimintaympäristön ja käytännöt.

Timanttisen unelmaduunin kannalta tästä seuraa keskeiset työelämätaidot ja asenteet, jotka tekijällä pitää olla. Se on sitten työnantajan valinta, pitääkö näiden taitojen olla hallussa ja asenteiden kohdallaan valmiiksi vai antaako työyhteisö mahdollisuuden opetella näitä taitoja ja kasvaa yrityksen toimintakulttuuriin.

Ilmarisen selvityksen ([[https://www.ilmarinen.fi/uutishuone/arkisto/2017/uudistu-tai-katoa/]]) perusteella tärkeimmät työelämätaidot ovat
1. Vuorovaikutustaito
2. Itsetuntemus
3. Tunneälykkyys ja empatia
4. Kyky tunnistaa ja kehittää omaa osaamista
5. Verkostoitumiskyky
6. Resilienssi eli muutosjoustavuus
7. Yhteistyökyky
8. Kyky toimia erilaisissa ympäristöissä ja kulttuureissa
9. Kriittinen ajattelu ja luovuus
10. Itseohjautuvuus

Äkkiseltään varsin hyvältä ja asialliselta tuntuva lista. Tarkemmin ajatellen herää kommentteja ja kysymyksiä:
- Ovat varsin abstrakteja käsitteitä. Mitähän niillä käytännössä tarkoitetaan?
- Osa noista taatusti vaaditaan siksi, ettei organisaation johtamis- ja toimintakulttuuri ole kunnossa. Missä määrin ja miten ymmärrettynä noita vaaditaan ideaalisessa yrityksessä?

Esimerkiksi itseohjautuvuus voisi tarkoittaa, ettei jää ihmettelemään ongelmallista tilannetta vaan tekee mitä parhaaksi näkee, jotta tuotanto pysyy käynnissä. Jos yksikin toimii näin, niin organisaatio on kaoottisessa tilassa (ks. kohta portaat) ja sen on hyvin vaikea kehittyä tuottavammaksi ilman perusteellista toimintakulttuurin remonttia. Toki ideaalisessakin organisaatiossa joutuu sopeutumaan ja improvisoimaan joskus, mutta se tehdään ohjaajan hyväksynnän ja valvonnan alaisena, jotta ongelmatilanne tulee päätösvaltaisten tietoon ja sen syy voidaan poistaa.

Vuorovaikutustaitojenkin merkitystä voidaan liioitella. Jos organisaatiossa on kilpailun kulttuuri, jossa ideoiden ja parannusehdotusten tulevaisuus riippuu esittäjänsä vuorovaikutustaidoista, taatusti merkittäviä ongelmia ja mahdollisuuksia jää huomaamatta ja hyödyntämättä. Eli ehkä se tärkein vuorovaikutustaito on, että osaa kuunnella myös sellaisia, joiden itseilmaisun taidot eivät edusta valtavirtaa.

Vuorovaikutustaitojen tarvetta voi myös minimoida. Välttämätön on helppo ja persoonaton tapa sanoa, että nyt minulla on ongelma, joka hidastaa tai estää jatkamasta tuotantoa (andon call).

Toinen välttämättömyys on myötämielinen asenne ongelmien ratkaisemista ja oppimista kohtaan. Jos joku sanoo, että tuuppasit muuten kuraa arvovirtaan, niin ei kiellä/torju, ei selittele eikä syyllisty vaan alkaa omalta osaltaan selvittää, mitä loppujen lopuksi on tapahtunut ja mitä pitää tehdä toisin, että sama ongelma ei toistu enää ikinä.

Yhteistyön edellytyksiä miettiessä täytyy ottaa kantaa myös etätyöhön ja mahdollisesti hajautettuun organisaatiorakenteeseen. Sinänsä nykyaikainen teknologia mahdollistaa F2F keskusteluja tehokkaamman, rinnakkaistetun, häiritsemättömän, ajasta ja paikasta riippumattoman vuodovaikutuksen ja yhteistyön, joten on kannattavaa rakentaa etätyön mahdollistava toimintaympäristö ainakin tietotyöläisille, vaikka toiminta lähtökohtaisesti tapahtuisikin toimistolla säännöllisen työajan puitteissa.

** Joukko prosesseja

Yhden määritelmän mukaan projekti on joukko asioita, jotka kaikki pitää tehdä, että saadaan aikaan toivottu lopputulos. Prosessi puolestaan on joukko keskenään samanlaisia projekteja.

Liiketoimintaan liittyviä prosesseja ovat esimerkiksi
- Tuotanto- ja tukipalveluprosessit, joiden lopputulos on asiakkaan
  saama lisäarvo
- (Talous)hallintoon liittyvät prosessit tuottavat esim. ne liiketoiminan tilaa kuvaavat dokumentit, jotka toimitetaan viranomaisille, omistajille, tuotannonohjauksesta vastaaville jne.
- Tuotekehitysprosessit kehittävät tuotteita ja niihin liittyviä tuotantoprosesseja.
- Markkinointi-, myynti-, rekrytointi-, ja ostoprosessit luovat ja ylläpitävät niitä kanavia, joita pitkin, asiakkaat tavoitetaan, lisäarvo toimitetaan asiakkaille sekä löydetään uudet työntekijät, sijoittajat ja raaka-aineiden toimittajat.
- Liiketoiminnan kehittämisprosessit ylläpitävät tilannekuvaa markkinasta ja luovat aihioita arvoinnovaatioiksi ja ehdotuksia strategian muuttamisesta.
- Johtamisen ja oppimisen metaprosessit, joilla ylläpidetään ja kehitetään muita prosesseja.

Tämä prosessinäkökulma on sikäli tärkeä, että siitä näkee yksityiskohtaisesti mitä yrityksessä tehdään, millaisia rooleja on ja mitä osaamista missäkin roolissa tarvitaan. Prosessit myös ovat niitä entiteettejä, jotka määräävät yrityksen kyvykkyyden ja joita kehittämällä kyky tuottaa lisäarvoa kasvaa.

Prosesseihin liittyy läheisesti standardit. Tosin tässä yhteydessä standardi ei tarkoita kiveenhakattuja vaatimuksia, jotka työn kulun tai lopputuloksen täytyy täyttää. Ennemminkin kysymys on yksityiskohtaisesti dokumentoidusta suunnitelmasta, jonka mukaisesti toimien uskotaan homman hoituvan. Standardi on siis vertailukohta, jonka avulla tunnistetaan työhön ja työympäristöön liittyviä asioita, joita ei suunnitteluvaiheessa oltu huomattu ja tajuttu.

* Alustava etsintäsuunnitelma

1. Rakennetaan etsintää varten kumppanuus- ja vertaisverkosto, koska timanttisen unelmaduunin etsiminen ei ole yhden naisen eikä miehen homma. Eli jos hanke kiinnostaa, niin pyydä kavereitasikin mukaan.

2. Hankitaan ja rakennetaan vuorovaikutuksen ylläpitämisessä tarvittavat välineet, yhteistyöalustat ja tilat sekä kehitetään yhteistyön mahdollistavat vuorovaikutuksen käytännöt ja pelisäännöt.

3. Muokataan ja tarkennetaan kuvaa ideaalisesta yrityksestä, jotta kukin voi peilata omia haaveitaan sitä vasten. Sen lisäksi, että tietää mitä etsii, on kiva olla kohtuullisen varma, että etsitty asia on reaalimaailmassakin olemassa.

4. Käynnistetään markkinoinnin ja liiketoiminnan kehittämisen prosesseja, jotta löydetään
   - asiakkaita, joille halutaan tuottaa lisäarvoa
   - jo olemassa olevia yrityksiä, jotka palvelevat näitä asiakkaita
   - uusia arvoinnovaatioita, joitka mahdollistavat näiden asiakkaiden palvelelmisen vieläkin paremmin.

5. Jos löytyy ehdot täyttäviä olemassa olevia organisaatioita, polkaistaan käyntiin kohdennettuja työnhakukampanjoita. Ellei löydy tai ne eivät työllistä, niin tutkitaan mahdollisuutta uuden yrityksen perustamiseen tai palataan takaisin edelliseen kohtaan jatkamaan uusien asiakasryhmien etsimistä.


Kaikki kommentit ja kysymykset ovat enemmän kuin tervetulleita. Osallistu keskusteluun tämän projektin kotipesässä:
[[https://gitlab.com/heikkibvirtanen/ideaalinen-yritys/issues]]

Tai lähetä viestiä sitä kanavaa pitkin, jonka kautta sait tiedon tästä projektista.

* Täydentäviä tietoja ja selityksiä

** Toimintakulttuurin kehityksen <<<portaat>>>

Lähde: Sari Torkkola: /Lean asinatuntijatyön johtamisessa/: Luku 6: ``Oivallus: Kaaosta ei voi parantaa''

*** Kaoottinen porras

Keskeinen tunnusmerkki tälle portalle on, että kaikki tekevät sitä, mikä heidän omasta mielestään on oikein.

*** Järjestäytynyt porras

Tällä portaalla käytössä on yhteiset menettelytavat, ja kaikki organisaatioon kuuluvat työskentelevät yhdessä kohti yhteistä päämäärää.

*** Stabiili porras

Tässä vaiheessa toistoja on takana niin paljon, että toimintaympäristön lainalaisuudet ovat tulleet tutuiksi ja erilaisten muutosten vaikutuksia osataan ennustaa ja ennakoida. Eli myös suorituskyky tunnetaan ja siinä esiintyvä vaihtelu on luonnollista satunnaisuutta.

*** Optimoitu porras

Ylin tavoite, missä asiakastarpeet odotetaan sopimusten tai muiden tekijöiden kautta tunnetuiksi ja tekeminen on optimoitu näiden tarpeiden mukaan.
